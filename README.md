# best_practices

Collection of best practices

## Document organization

### File/Folder naming best practices:
* Files and folders should be named consistently
* File and folder names should be short but descriptive (less than 25 characters) (Briney)
* Avoid special characters or spaces in a file name
* Use capitals and underscores instead of periods or spaces or slashes
* Use date format ISO 8601: YYYY-MM-DD
    * If some file/folder belongs to a range of days, indicate the range by '--' e.g. YYYY-MM-DD--DD or 2019-12-30--2020-01-01 if the year/month digits change.
* Include a version number (Creamer et al.)
* Write down naming convention in data management plan

### Elements to consider using in a naming convention are:
* Date of creation (putting the date in the front will facilitate computer aided date sorting)
* Short Description
* Work
* Location
* Project name or number
* Sample
* Analysis
* Version number (Always consider proper version control! Prefered: git/dvc)

### Philosophy and Examples
If chronological ordering is sensible, file names are named like
"YYYY-MM-DD_CamelCaseOverarchingTag_CamelCaseSubTag_TamelCaseSubSubTag_versionIfNecessary.ext"

Examples:
* "Figures"
    * "2020-07-01\_NEISS\_EuLogo_original.png"
    * "2020-07-01\_NEISS\_EuLogo_cropped.jpeg"
* "Talks"
    * "2020-07-02\_NEISS_BestPractices.pptx"
    * "2019-12-30--2020-01-01_SummerSchoolOnMachineLearning"
        * "2019-12-30_Introduction.pptx"
        * "2020-01-01_Summary.pptx"

### References:
    https://mantra.edina.ac.uk/organisingdata/
    https://acrl.ala.org/techconnect/post/an-elevator-pitch-for-file-naming-conventions/
    https://www.youtube.com/watch?v=3MEJ38BO6Mo&feature=emb_logo

## Python Programming:
### Intuitive Polymorphisms:
* Plotting: If classes can be visualized the should have a method 'plot' with the signature defined by rna.plotting.PlotManager. A plotmanager should be used for handling the arguments. The method returns an artist or list of artists. This allows easy addition of colorbars with rna.plotting.colorbar.



